# Programsko inženjerstvo – projektni zadatak

## Opis aplikacije 

**Web trgovina za prodaju nogometne opreme - Ivan Aleksić**

Web trgovina za prodaju nogometne opreme predstavlja aplikaciju koja omogućuje naručivanje nogometne opreme. Web aplikacija omogućuje prijavu korisnika, kojom postaje član (_Member_) i na taj način ostvaruje neke od pogodnosti, odnosno popuste na određene proizvode na mjesečnoj bazi, koje nudi Web trgovina. Također, prijavljeni korisnik ima uvid u sve svoje provedene narudžbe. Proizvode koje nudi Web trgovina moguće je naručiti kao anonimni korisnik, odnosno ne-ulogirani korisnik, ali i kao prijavljeni korisnik. 

Prikaz početne stranice web aplikacije sadrži navigacijsku traku u kojoj se nalazi logo web trgovine (klikom gumba našeg pokazivača na logo vraćamo se svaki put na početnu stranicu), područje za pretraživanje proizvoda prema njihovom definiranom nazivu spremljenog u bazu podataka, gumb za prijavu korisnika te alat za prikaz trenutnog stanja količine dodanih proizvoda i ukupna cijena svih dodanih proizvoda u košaricu. 

Klikom gumba našeg pokazivača na ikonu košarice, otvara se novo sučelje u vidu tablice koja nudi prikaz svih trenutno dodatnih proizvoda unutar web košarice s općim informacijama o proizvodu (naziv, cijena i slika), mogućnost povećavanja, odnosno smanjivanja količine za pojedini proizvod te brisanje samog proizvoda iz košarice. Na dnu tablice nalaze se informacije o ukupnoj količini dodanih proizvoda unutar košarice, ukupnoj cijeni i informacija o besplatnoj dostavi. Ispod navedenih stavki o važnim informacijama, nalazi se gumb koji vodi na sučelje za popunjavanje forme o korisničkim podacima, poput osnovnih podataka o kupcu, adrese za naplatu i dostavu naručenih proizvoda. Nakon čega je opet ispisan ukupni pregled svih dodatnih proizvoda te ukupna cijena, poslije čega je moguće odabirom gumba Purchase obaviti narudžbu, nakon kojeg dobijemo ispis poruke kako je kupnja uspješno obavljena. 

Za prijavljenog korisnika unutar navigacijske trake pojavljuje se simbolična poruka kojom se daje do znanja da je korisnik uspješno prijavljen unutar web aplikacije. Također, prijavom korisnika, pojavljuju se i gumbi Member te Orders kojima prijavljeni korisnik može pristupiti popustima na određenim proizvodima, odnosno pristupiti svim narudžbama koje je do sada izvršio. 

Na lijevom dijelu prikaza početne stranice nalazi se izbornik sa svim kategorijama koje nudi web trgovina, a predstavlja glavnu navigaciju do traženja željenog proizvoda. U zavisnosti o odabiru kategorije otvaraju se nove stranice koje nude popis svih proizvoda iz baze podataka za svaku specifičnu kategoriju. 

Glavni dio početne stranice se sučelje koje nudi popis svih proizvoda za određenu kategoriju, gdje se nalaze slike proizvoda, naziv i cijena te gumb za dodavanje proizvoda u košaricu.

Klikom gumba na sliku ili naziv pojedinog proizvoda, otvara se nova stranica koja sadrži povećanu i jasniju sliku odabranog proizvoda, gumb za dodavanje u košaricu, opis proizvoda te gumb za vraćanje na stranicu s popisom svih proizvoda iz birane kategorije. 

## Ograničenja

- **Anonimni korisnici** (ne-ulogirani) imaju sljedeći pristup stranici: 
    1. Pristup svim proizvodima koji se nalaze u web shopu
    2. Pretraživanje svih proizvoda web shopa
    3. Dodavanje proizvoda u košaricu
    4. Povećavanje količine proizvoda ili brisanje proizvoda iz košarice
    5. Narudžba proizvoda

- **Prijavljeni korisnici** (ulogirani) imaju isti pristup kao i anonimni korisnici uz napomenu da prijavljeni korisnici svojom prijavom na web shop postaju _Member-i_ čime mogu ostvariti dodatne pogodnosti kod naručivanja proizvoda iz web shopa. Također, prijavljeni korisnici imaju uvid u sve svoje narudžbe do sada (id narudžbe, ukupna cijena, ukupno naručenih proizvoda te datum kada je obavljena narudžba).

- Karakteristično administratorsko sučelje/stranica (_Admin Dashboard_) ne postoji, ali administrator stranice unutar baze podataka (pomoću pgAdmin sučelja, budući da je baza podataka PostgreSQL) može kreirati nove proizvode ili kategorije proizvoda, čitati sve podatke unutar baze podataka, ažurirati tablice novim podacima ili ih brisati

## Persone

Persone označavaju tipove korisnika iz pogleda dizajna sučelja. Ovlasti i motivi persona se mijenjaju u ovisnosti o tome koji je cilj određene persone te na kojem se dijelu aplikacije nalazi.

- **Anon** – _Visitor_ aplikacije koji nije ulogiran, ali može naručivati proizvode.
- **User** – Korisnički račun koji je ulogiran u aplikaciju. Samom prijavom postaje _Member_ te ima uvid u sve svoje narudžbe do sada. 
- **Member** – Korisnički račun koji je ulogiran u aplikaciju i kojem su omogućeni dodatni popusti na proizvode.
- **Anyone** – Persona koja predstavlja sve ostale persone. Funkcionalnost koja joj je dozvoljena, dozvoljena je i ostalim personama. 

NAPOMENE:
1. Korisnički račun administratora (Admin persona) ne postoji, ali podrazumijeva se administrator/kreator stranice ima sve ovlasti nad bazom podataka (kreiranje, čitanje, ažuriranje ili brisanje podataka)
2. Sve korisničke račune moguće je prikazati pomoću User modela. Odnosno, privilegije ostalih rola ovise o tome koji dio aplikacije pregledavaju. Persone nisu odvojeni modeli, već služe da posjetitelju aplikacije olakšaju viziju aplikacije. Jedan korisnički račun može imati i više Persona, budući da je korisniku omogućeno naručivanje proizvoda i bez prijave u aplikaciju, ali i s prijavom kojom postaje _Member_ te ostvaruje dodatne pogodnosti. 

Primjer: Ukoliko se radi o roli _User_, tada se ona odnosi na korisnički račun koji je prijavljen u aplikaciju i koji ima mogućnosti dodavanja i naručivanja proizvoda iz košarice (kao i rola _Anon_), ali i dodatne mogućnosti kao što su popusti na određene proizvode i uvid u sve obavljene narudžbe do sada (rola _Member_). 


## Stranice

Dio aplikacije koji predstavlja osnovne stranice koje su korištene kroz projekt. 

NAPOMENA: 
Pojedine stranice će također biti potrebne kako bi se implementirale određene funkcionalnosti, ali nisu ovdje izričito navedene budući da se podrazumijevaju.

Primjer: Stranica za upisivanje emaila i lozinke za prijavu korisnika nije navedena, ali podrazumijeva se njeno postojanje, jer je na neki način omogućena prijava korisnika. Isto vrijedi i za ostale slične stranice.

- HOMEPAGE (PRODUCTS) stranica – označava početnu stranicu aplikacije. 
Sadrži:
    1. Navigacijsku traku u kojoj se nalazi logo web shopa, search bar, login/logout button, member i order button (s uvjetom da je korisnik    prijavljen), cart-status alat za prikaz količine i cijene dodanih proizvoda (ikona košarice).
    2. Product category menu u kojem se nalazi popis svih kategorija proizvoda koje web shop posjeduje. Klik na određenu kategoriju daje nam listu proizvoda te odabrane kategorije. 
    3. Product list u kojem se nalazi popis svih proizvoda za određenu odabranu kategoriju

- MEMBER_PAGE stranica – stranica u kojoj će biti prikazani popusti na određene proizvode za prijavljenog korisnika. Na ovu stranicu odlazimo klikom na button Member.
- ORDER_HISTORY stranica – stranica na kojoj je prikazana tablica sa svim detaljima o narudžbama koje je radio prijavljeni korisnik aplikacije. Na ovu stranicu odlazimo klikom na button Orders.
- CART_DETAILS stranica – stranica na kojoj je prikazana tablica s trenutno dodanim proizvodima u košaricu, kojima je moguće mijenjati kvantitetu ili ih izbrisati iz košarice te ukupna cijena i ukupna količina dodanih proizvoda. Na ovu stranicu odlazimo klikom na ikonu košarice.
- SEARCH stranica – stranica koju dobijemo klikom na search button ukoliko pretražujemo neki određeni proizvod prema njegovom pripadajućem nazivu u bazi podataka.
- PRODUCT_DETAILS stranica – stranica na koju odlazimo klikom na sliku ili naziv određenog proizvoda iz liste proizvoda. Na stranici se je prikazana uvećana slika odabranog proizvoda, njegov naziv, button za dodavanje u košaricu, opis proizvoda te button za povratak na stranicu liste proizvoda.
- CHECKOUT stranica – stranica na kojoj se nalazi forma za popunjavanje podataka o kupcu, adresa dostave i naplate, kartici te button kojim potvrđujemo narudžbu.


# User stories

Epic user story može biti podijeljen na nekoliko user storyja, prema kojima su definirane funkcionalnosti na kojima se bazira aplikacija iz pogleda svake od navedenih persona. Svaki od User storyja ima određen acceptance criteria koji naposljetku potvrđuje uspješno ispunjavanje tog User storyja. Epic User story će biti ispunjen tek kada su ispunjeni svi acceptance kriteriji iz svih storija definiranih u zadanom Epicu. 

NAPOMENA: 
Neki User storyji će imati i polje Need. Prema tom polju navedeni su neki od zahtjeva za funkcionalnost aplikacije koji se ne mogu zaključiti samim tekstom pojedinog storyja. Tamo gdje nema polja Need, potrebni resursi za uspješno ispunjavanje storyja mogu se logički i s razumijevanjem lako zaključiti. 

Primjer: Ukoliko nam story govori kako na početnoj stranici postoji mogućnost za prijavu korisnika, podrazumijeva se da će anonimni korisnik morati unijeti svoj email i lozinku kako bi se ulogirao i kako bi ga sustav redirectao ponovno na početnu stranicu aplikacije. 


## Epic 1: Anon se može prijaviti u sustav web shopa

- S1-1 Kao Anon, kada pristupim HOMEPAGE (PRODUCTS) stranici, mogu prijaviti korisnički račun, nakon čega me sustav automatski redirecta nazad na HOMEPAGE stranicu

    NAPOMENA: 
    Funkcionalnost prijave korisnika u sustav web shopa je implementirana pomoću Okta Dashboarda, gdje administrator web shopa ima uvid u sve       prijavljene korisnike.

    Need: ispravan email (username) i password

    Acceptance criteria:

    - Anon vidi button za login u sustav web shopa
    - Klik na button za login otvara formu za prijavu korisnika
    - Anon se može prijaviti u aplikaciju unosom podataka (email i password)
    - Anon je redirectan nazad na HOMEPAGE nakon uspješne prijave

- S1-2 Kao User, kada pristupim HOMEPAGE (PRODUCTS) stranici postajem Member, imam uvid u sve svoje narudžbe i omogućena mi je odjava iz sustava web shopa

    Acceptance criteria:

    - User vidi button Member
    - Klik na button Member otvara stranicu s pogodnostima za Membera
    - User vidi button Orders
    - Klik na button Orderes otvara stranicu s popisom svih narudžbi korisnika
    - User vidi button Logout
    - Klik na button Logout odjavljuje Usera iz sustava, nakon čega ponovno postaje anonimni korisnik na stranici te ga redirecta na HOMEPAGE stranicu


## Epic 2: Anon i User vide listu proizvoda i ostale funkcionalnosti na početnoj stranici

- S1-1 Kao User ili Anon, kada pristupim početnoj stranici HOMEPAGE (PRODUCTS) vidim listu svih proizvoda koje nudi web shop, listu kategorija proizvoda, logo web shopa, search bar, login button, prikaz stanja u košarici te paginaciju na dnu stranice

    Need: Slika za logo, ikona košarice

    Acceptance criteria:

    - User ili Anon vidi logo web shopa
    - Svakim klikom na logo User ili Anon je redirectan na početnu stranicu
    - User ili Anon vidi search bar
    - Klikom na search bar može unijeti pojam (naziv proizvoda)
    - User ili Anon vidi login button
    - User ili Anon vidi stanje i ukupnu cijenu proizvoda u košarici 
    - Klik na ikonu košarice vodi na stranicu CART_DETAILS
    - User ili Anon vidi listu svih kategorija proizvoda 
    - Klik na pojedinu kategoriju otvara novu stranicu proizvoda koji se odnose na pojedinu kategoriju
    Primjer: Klik na kategoriju FOOTBALL BOOTS daje korisniku ispis svih proizvoda (kopačke) iz baze podataka za tu određenu kategoriju
    - User ili Anon vidi listu proizvoda (po defaultu je odabrana uvijek prva kategorija proizvoda prema listi proizvoda) s pripadajućom slikom, nazivom i buttonom za dodavanje u košaricu
    - Klik na sliku ili naziv pojedinog proizvoda, korisnika odvodi na stranicu s detaljima proizvoda (PRODUCT_DETAILS)
    - Klik na button „Add to cart“ omogućuje dodavanje proizvoda u košaricu, što je vidljivo povećavanjem cijene i kvantitete u alatu koji to prikazuje u navbaru
    - User ili Anon vidi alate za mijenjanje stranice i postavljanje ukupnog broja proizvoda koje će stranica prikazati za pojedinu kategoriju (po defaultu je 5)
    - Klik na pojedini broj stranice u paginaciji, korisnika odvodi na stranicu gdje su mu izlistani proizvodi iz baze podataka za tu kategoriju koju je odabrao, također klikom na alat za mijenjanje ukupnog broja proizvoda koji će biti prikazani na stranici može promijeniti ukupni broj proizvoda i time smanjiti ili povećati broj stranica u paginaciji

- S2-2 Kao User, kada pristupim početnoj stranici HOMEPAGE (PRODUCTS) vidim dodatne buttone za membera i orders

    NAPOMENA: 
    Funkcionalnost member i orders buttona objašnjena je u EPIC 1 storyju (S1-2)


## Epic 3: Anon i User mogu pretraživati proizvode po kategorijama ili direktnim upisom naziva proizvoda u search bar

- S3-1 Kao User ili Anon, kada pristupim početnoj stranici HOMEPAGE (PRODUCTS) vidim listu kategorija svih proizvoda iz baze podataka

    Acceptance criteria:

    - User ili Anon s lijeve strane na početnoj stranici ispod logoa web shopa vidi listu kategorija za sve proizvode iz baze podataka (ukupno 5    kategorija)
    - Klik na pojedinu kategoriju korisnika odvodi na stranicu proizvoda koji se odnose na tu kategoriju 

- S3-2 Kao User ili Anon, kada pristupim početnoj stranici HOMEPAGE (PRODUCTS) vidim search bar i button kojim se potvrđuje pretraga kroz sve proizvode za određeni uneseni pojam

    Acceptance criteria:

    - User ili Anon u navbaru na početnoj stranici web shopa vidi search bar 
    - Klik na search bar korisniku omogućuje da unese naziv proizvoda kojeg želi pronaći 
    - User ili Anon u navbaru na početnoj stranici pokraj search bara vidi button search kojim potvrđuje uneseni pojam koji želi pretraživati
    - Klik na button search korisniku daje ispis svih proizvoda s nazivom koji je unio u search bar


## Epic 4: Anon i User imaju uvid u detalje određenog proizvoda

- S4-1 Kao User ili Anon mogu pristupiti detaljima za svaki pojedini proizvod iz baze podataka

    Need: ikona košarice, ikona koja prikazuje strelicu u lijevo (povratak nazad)

    Acceptance criteria:

    - User ili Anon na početnoj stranici vidi popis proizvoda za prvu (po defaultu) odabranu kategoriju, odnosno vidi sliku proizvoda, naziv    proizvoda i button za dodavanje proizvoda u košaricu
    - Klik na sliku ili naziv proizvoda korisniku daje detalje o pojedinom proizvodu, odnosno uvećanu sliku proizvoda, naziv i cijenu proizvoda,    gumb za dodavanje u košaricu, opis proizvoda te link za povratak na listu s proizvodima

- S4-2 Kao User ili Anon kada pristupim stranici s detaljima pojedinog proizvoda vidim dodatne buttone i ikone

    Need: ikona košarice, ikona koja prikazuje strelicu u lijevo (povratak nazad)

    Acceptance criteria:

    - User ili Anon na stranici s detaljima proizvoda vidi button za dodavanje proizvoda u košaricu (Add to cart)
    - Klik na button Add to cart dodaje proizvod u košaricu što je vidljivo u navbaru mijenjanjem količine i ukupnog broja proizvoda u košarici 
    - User ili Anon na stranici s detaljima proizvoda vidi link za povratak na stranicu s ispisom proizvoda
    - Klik na link za povratak nazad, korisnika odvodi na stranicu na kojoj se prethodno već nalazio, odnosno na stranicu s popisom svih proizvoda za određenu kategoriju 


## Epic 5: Anon i User imaju uvid u proizvode koje su dodali u košaricu

- S5-1 Kao User ili Anon kada pristupim početnoj stranici vidim navbar u kojem se nalazi ikona košarice i trenutno stanje košarice (ukupni broj dodanih proizvoda, ukupna cijena)

    Acceptance criteria:

    - User ili Anon na početnoj stranici vidi detalje o trenutnom stanju u košarici, odnosno vidi trenutni ukupni broj dodanih proizvoda te ukupnu cijenu ukoliko bi se odlučio naručiti te proizvode, pokraj tih informacija nalazi se ikona košarice
    - Klik na ikonu košarice korisnika odvodi na CART_DETAILS stranicu

- S5-2 Kao User ili Anon kada pristupim CART_DETAILS stranici vidim tablicu u kojoj se nalaze podaci o dodanim proizvodima u košaricu, odnosno slika proizvoda, naziv i cijena proizvoda, buttoni za povećavanje, odnosno smanjivanje količine pojedinog proizvoda, remove button te checkout button, također u donjem dijelu tablice ispisani su podaci o ukupnoj količini dodanih proizvoda, informacija o besplatnoj dostavi te ukupna cijena narudžbe

    Need: ikona plus, ikona minus, ikona križić

    Acceptance criteria:

    - User ili Anon na stranici s detaljima o košarici vidi tablicu s općim detaljima te dodatnim buttonima
    - Klik na button za povećavanje količine proizvoda omogućuje korisniku povećavanje količine pojedinog proizvoda u košarici, što je vidljivo i prikladnim povećavanjem brojeva pokraj buttona
    - Klik na button za smanjivanje količine proizvoda omogućuje korisniku smanjivanje količine pojedinog proizvoda u košarici, što je vidljivo i prikladnim smanjivanjem brojeva pokraj buttona (ukoliko je broj na 1 i kliknemo button za smanjivanje količine, proizvod će biti uklonjen iz košarice)
    - Klik na button Remove, briše određeni proizvod iz košarice (bez obzira koliko ga ima količinski)
    - Klik na button Checkout, korisnika vodi na stranicu za ispunjavanje forme o narudžbi (ostavljanje osobnih podataka o njemu, upisivanje brojeva kreditne kartice, adrese za dostavu i naplatu)


## Epic 6: Anon i User mogu naručiti proizvode ispunjavanjem forme za narudžbu

- S6-1 Kao User ili Anon kada pristupim stranici s detaljima košarice (CART_DETAILS stranica) vidim button Checkout koji me vodi na CHECKOUT stranicu

    Acceptance criteria:

    - User ili Anon na Checkout stranici vidi formu koju je potrebno popuniti kako bi se narudžba smatrala uspješnom te button Purchase za potvrdu narudžbe
    - Forma se sastoji od više različitih dijelova, prvi dio je nazvan Customer i sadrži polja koja je potrebno popuniti podacima s imenom i prezimenom korisnika te emailom (polje s emailom bit će automatski ispunjeno ukoliko je korisnik prijavljen) koji će biti spremljeni u bazu podataka
    - Drugi dio je Shipping Address i sadrži dvije dropdown liste za odabir države i županije, odnosno pokrajine, polja za upis ulice, grada i poštanskog broja za adresu dostave koja će biti spremljena u bazu podataka
    - Treći dio je Billing Address i sadrži ista polja kao i Shipping Adress

    NAPOMENA:
    Ukoliko će Billing Address i Shipping Address sadržavati identične podatke korisniku je omogućen checkbox kojim će potvrditi identičnost podataka o adresi dostave i adresi naplate
    - Četvrti dio je Credit Card koji sadrži dropdown liste za odabir tipa kartice (Mastercard ili Visa) te datum i godinu isteka valjanosti kartice, također tu su i polja koja je potrebno popuniti imenom korisnika na kartici, brojem kartice i sigurnosnim brojem/kodom kartice
    - Peti dio je Rewiev Your Order koji daje sve podatke o narudžbi, odnosno koliko se ukupno proizvoda nalazi u košarici, informacija o besplatnoj dostavi i ukupna cijena narudžbe
    - Klikom na button Purchase obavlja se narudžba proizvoda i dobiva se povratna poruka u vidu skočnog prozora koji potvrđuje narudžbu i ispisuje Order tracking number


## Epic 7: User ima Member access web shopu

- S7-1 Kao User kada se prijavim u sustav web shopa dobivam specijalne pogodnosti kao Member stranice

    Acceptance criteria:

    - User nakon prijave u sustav web shopa dobiva dodatni button Member
    - Klikom na button Member otvara se stranica na kojoj su potencijalni popusti na određene proizvode ili poruka kako popusti za Membere dolaze uskoro


## Epic 8: User ima pristup svim njegovim dosadašnjim narudžbama

- S8-1 Kao User kada se prijavim u sustav web shopa dobivam uvid u sve svoje narudžbe 

    Acceptance criteria:

    - User nakon prijave u sustav web shopa dobiva dodatni button Orders
    - Klikom na button Orders otvara se stranica na kojoj se nalazi tablica sa svim narudžbama za prijavljenog korisnika koje je do sada obavio,
     u tablici su sljedeći podaci: Order Tracking number, Total Price, Total Quantity i Date


## UML dijagram  

![se-project-diagram](/uploads/16b155c822d3e030b3dc6ba08a7ca30f/se-project-diagram.png)


